package com.dododo.studyjava.service;

import com.dododo.studyjava.model.Example1Item;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
public class Example1Service {
    /**
     *  샘플 데이터를 생성한다.
     * @return  Example1item 리스트
     */
    private List<Example1Item> settingData() {
        List<Example1Item> result = new LinkedList<>(); // 빈 Example1Item의 리스트 생성

        Example1Item item1 = new Example1Item(); // Example1Item 리스트에 Example1Item item1을 생성
        item1.setName("고길동"); // Item1에 고길동 이름을 넣었다
        item1.setBirthday(LocalDate.of(2022, 1, 1)); // item1의 생일은 2022년 1월 1일이다.
        result.add(item1); // item1을 Example1Item 리스트에 넣는다

        Example1Item item2 = new Example1Item(); // Example1Item 리스트에 Example1Item item2를 생성
        item2.setName("둘리"); // Item2에 둘리 이름을 넣었다
        item2.setBirthday(LocalDate.of(2022, 2, 2));
        result.add(item2);

        Example1Item item3 = new Example1Item(); // Example1Item 리스트에 Example1Item item3을 생성
        item3. setName("희동이"); // Item3에 희동이 이름을 넣었다
        item3. setBirthday(LocalDate.of(2022,3,3));
        result.add(item3);

        Example1Item item4 = new Example1Item(); // Example1Item 리스트에 Example1Item item4를 생성
        item4. setName("마이콜"); // Item4에 마이콜 이름을 넣었다
        item4. setBirthday(LocalDate.of(2022,4,4));
        result.add(item4);

        Example1Item item5 = new Example1Item(); // 빈 Example1Item의 리스트 생성
        item5. setName("또치"); // Item5에 또치 이름을 넣었다
        item5. setBirthday(LocalDate.of(2022,5,5));
        result.add(item5);


        return result;

    }
}
